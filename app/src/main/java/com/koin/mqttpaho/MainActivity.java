package com.koin.mqttpaho;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button btn_connect,btn_publish,btn_subscribe,btn_disconnect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();

        btn_connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyMqttUtil.getInstance(MainActivity.this).connectServer();
            }
        });

        btn_publish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyMqttUtil.getInstance(MainActivity.this).pubMessage("Topic_Pub", "123456");
            }
        });

        btn_subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyMqttUtil.getInstance(MainActivity.this).subTopic("Topic_Pub", 1);
            }
        });

        btn_disconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyMqttUtil.getInstance(MainActivity.this).disConnectServer();
            }
        });
    }


    private void init(){
        btn_connect = findViewById(R.id.btn_connect);
        btn_publish = findViewById(R.id.btn_publish);
        btn_subscribe = findViewById(R.id.btn_subscribe);
        btn_disconnect = findViewById(R.id.btn_disconnect);

    }


}